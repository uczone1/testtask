import { MigrationInterface, QueryRunner } from 'typeorm';

export class initMigration1633041121481 implements MigrationInterface {
  name = 'initMigration1633041121481';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "transactions" ("id" SERIAL NOT NULL, "transactionId" bytea NOT NULL, "from" bytea NOT NULL, "to" bytea, "value" bytea NOT NULL, "blockId" integer, CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "blocks" ("id" SERIAL NOT NULL, "blockNumber" bytea NOT NULL, "timestamp" bytea NOT NULL, "transactionsRoot" bytea NOT NULL, "size" bytea NOT NULL, "parentHash" bytea NOT NULL, CONSTRAINT "PK_8244fa1495c4e9222a01059244b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "transactions" ADD CONSTRAINT "FK_e11180855c1afd8fe21f96a1bf8" FOREIGN KEY ("blockId") REFERENCES "blocks"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "transactions" DROP CONSTRAINT "FK_e11180855c1afd8fe21f96a1bf8"`,
    );
    await queryRunner.query(`DROP TABLE "blocks"`);
    await queryRunner.query(`DROP TABLE "transactions"`);
  }
}
