import * as dotenv from 'dotenv';
dotenv.config();


export const appConfig = {
  app: {
    port: +process.env.APP_PORT,
  },
  postgres: {
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
  },
  etherscan: [process.env.ETHERSCAN_API_KEY_1, process.env.ETHERSCAN_API_KEY_2],
};
