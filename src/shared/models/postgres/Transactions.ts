import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Blocks } from './Blocks';

@Entity('transactions')
export class Transactions extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'bytea', nullable: false })
  transactionId: string;

  @Column({ type: 'bytea', nullable: false })
  from: string;

  @Column({ type: 'bytea', nullable: true })
  to: string;

  @Column({ type: 'bytea', nullable: false })
  value: string;

  @ManyToOne(() => Blocks, (block) => block.transactions)
  block: Blocks;
}
