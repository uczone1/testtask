import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Transactions } from './Transactions';

@Entity('blocks')
export class Blocks extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'bytea', nullable: false })
  blockNumber: string;

  @Column({ type: 'bytea', nullable: false })
  timestamp: string;

  @Column({ type: 'bytea', nullable: false })
  transactionsRoot: string;

  @Column({ type: 'bytea', nullable: false })
  size: string;

  @Column({ type: 'bytea', nullable: false })
  parentHash: string;

  @OneToMany(() => Transactions, (transaction) => transaction.block, {
    cascade: ['insert', 'update', 'remove'],
  })
  transactions: Transactions[];
}
