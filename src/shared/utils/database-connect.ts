import { Connection, ConnectionOptions, createConnection } from 'typeorm';
import * as path from 'path';
import { appConfig } from '../config/config';


export const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  synchronize: false,
  host: appConfig.postgres.host,
  port: appConfig.postgres.port,
  username: appConfig.postgres.username,
  password: appConfig.postgres.password,
  database: appConfig.postgres.database,
  entities: [path.join(__dirname, '..', 'models', 'postgres', '*.*s')],
  logging: false,
  migrations: [path.join(__dirname, '..', 'migration', 'postgres', '*.*s')],
  cli: {
    entitiesDir: path.join(__dirname, '..', 'models', 'postgres', '*.*s'),
    migrationsDir: path.join(__dirname, '..', 'migration', 'postgres', '*.*s'),
    subscribersDir: path.join(
      __dirname,
      '..',
      'subscriber',
      'postgres',
      '*.*s',
    ),
  },
};

export async function pgConnect(): Promise<Connection> {
  return await createConnection(connectionOptions);
}
