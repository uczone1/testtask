import { Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CalculationModule } from './modules/main/calculation/calculation.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.env`,
    }),
    CalculationModule,
  ],
  controllers: [],
  providers: [Logger],
})
export class AppModule {}
