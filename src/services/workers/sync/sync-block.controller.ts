import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { SyncBlocksService } from './sync-blocks.service';

@Controller()
export class SyncBlockController {
  constructor(private _service: SyncBlocksService) {}
  @MessagePattern('dbReady')
  dbReady() {
    return this._service.FLAG_LOADED;
  }
  @MessagePattern('leftToCache')
  leftToCache() {
    return this._service.leftToCache();
  }
}
