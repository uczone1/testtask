import { Logger, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { SyncBlocksService } from './sync-blocks.service';
import { SyncBlockController } from './sync-block.controller';

@Module({
  imports: [ScheduleModule.forRoot()],
  providers: [SyncBlocksService, Logger],
  controllers: [SyncBlockController],
  exports: [SyncBlocksService],
})
export class SyncModule {}
