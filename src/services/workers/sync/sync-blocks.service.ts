import { Injectable, Logger } from '@nestjs/common';
import {
  Connection as DatabaseConnection,
  getConnection,
  Repository,
} from 'typeorm';
import { Transactions } from '../../../shared/models/postgres/Transactions';
import { Interval, SchedulerRegistry, Timeout } from '@nestjs/schedule';

import axios from 'axios';
import { Blocks } from '../../../shared/models/postgres/Blocks';

@Injectable()
export class SyncBlocksService {
  protected _db: DatabaseConnection;
  protected _repositoryTransactions: Repository<Transactions>;
  protected _repositoryBlocks: Repository<Blocks>;
  private _FLAG_LOADED = false;

  private _lastBlockNumber: number;
  private _queueProcessingBlocks: Array<string> = [];
  private _errorSyncBlocks: Array<string> = [];
  private _processingBlocks: Set<string> = new Set();
  private _loadedBlocks: Set<string> = new Set();

  constructor(
    protected readonly _logger: Logger,
    private _schedulerRegistry: SchedulerRegistry,
  ) {
    this.init();
  }

  set FLAG_LOADED(value: boolean) {
    this._FLAG_LOADED = value;
  }

  get FLAG_LOADED(): boolean {
    return this._FLAG_LOADED;
  }

  leftToCache() {
    return Math.max(0, 100 - this._loadedBlocks.size);
  }

  init() {
    this._db = getConnection();
    this._repositoryTransactions = this._db.getRepository(Transactions);
    this._repositoryBlocks = this._db.getRepository(Blocks);
  }

  @Timeout(0)
  async runStart() {
    await this.loadLastBlocks();
  }

  async loadLastBlocks() {
    try {
      const lastBlock = parseInt(await this.getLastBlockId(), 16);
      for (
        let currentBlockNum = lastBlock;
        currentBlockNum >= lastBlock - 100;
        currentBlockNum--
      ) {
        if (this._loadedBlocks.size >= 100) break;

        const blockNumber = '0x' + currentBlockNum.toString(16);

        if (
          !this._processingBlocks.has(blockNumber) &&
          !this._loadedBlocks.has(blockNumber)
        ) {
          this._processingBlocks.add(blockNumber);
          const alreadyBlock = await this._repositoryBlocks.findOne({
            where: {
              blockNumber: blockNumber,
            },
          });

          if (alreadyBlock) {
            this._processingBlocks.delete(blockNumber);
            this._loadedBlocks.add(blockNumber);
            continue;
          }
        }

        await this.getBlockById(-1, blockNumber);

        this._logger.log(
          `Loaded last 100 block: ${blockNumber} left: ${Math.max(
            0,
            100 - this._loadedBlocks.size,
          )}`,
        );
      }
      this._logger.log('Last 100 blocks loaded!');
      this.FLAG_LOADED = true;
    } catch (e) {
      console.log('CheckLoadBlocks: ', e);
    }
  }

  addBlockToQueue(blockId: string) {
    let lastNumber = 0;
    if (this._queueProcessingBlocks.length !== 0)
      lastNumber = parseInt(
        `0x${
          this._queueProcessingBlocks[this._queueProcessingBlocks.length - 1]
        }`,
      );
    else
      lastNumber = this._lastBlockNumber
        ? this._lastBlockNumber
        : parseInt(blockId) - 1;

    for (let i = lastNumber + 1; i <= parseInt(blockId); i++) {
      this._queueProcessingBlocks.push(i.toString(16));
    }
    this._lastBlockNumber = parseInt(blockId);
  }

  @Interval(200)
  async checkBlocksRealTime() {
    try {
      const blockId = await this.getLastBlockId();
      if (
        blockId &&
        !this._loadedBlocks.has(blockId) &&
        !this._processingBlocks.has(blockId)
      ) {
        this._processingBlocks.add(blockId);
        this.addBlockToQueue(blockId);
      }
    } catch (e) {
      this._logger.error(`SyncBlocksRealTime: ${e}`);
    }
  }

  @Interval(200)
  async processingSync() {
    if (this._queueProcessingBlocks.length > 0) {
      const blockId = this._queueProcessingBlocks.shift();
      const result = await this.getBlockById(-1, blockId);
      if (result) this._logger.log('New block: ' + result);
    }
    if (this._errorSyncBlocks.length > 0) {
      const blockId = this._errorSyncBlocks.shift();
      const result = await this.getBlockById(-1, blockId);
      if (result) this._logger.log('ReSync: ' + result);
    }
  }

  async getLastBlockId(): Promise<string | undefined> {
    try {
      const response = await axios({
        url: `https://api.etherscan.io/api?module=proxy&action=eth_blockNumber&apikey=${process.env.ETHERSCAN_API_KEY_1}`,
      });

      if (response.data.status == 0) return undefined;

      return response.data.result;
    } catch (e) {
      console.log('GetLastBlockId: ', e);
    }
  }

  async getBlockById(id: number, id_hex: string) {
    try {
      if (id !== -1) id_hex = '0x' + id.toString(16);

      if (this._loadedBlocks.has(id_hex)) return;

      const alreadySavedBlock = await this._repositoryBlocks.findOne({
        where: { blockNumber: id_hex },
      });

      if (alreadySavedBlock) {
        this._loadedBlocks.add(id_hex);
        this._processingBlocks.delete(id_hex);
        return;
      }

      const response = await axios({
        url: `https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=${id_hex}&boolean=true&apikey=${process.env.ETHERSCAN_API_KEY_2}`,
      });

      const {
        result: {
          number: blockId,
          transactions: transactions,
          transactionsRoot,
          size,
          parentHash,
          timestamp,
        },
      } = response.data;

      if (!blockId || !transactions) {
        this._logger.error(response.data);
        throw undefined;
      }

      const blockEntity = new Blocks();
      blockEntity.blockNumber = blockId;
      blockEntity.transactionsRoot = transactionsRoot;
      blockEntity.size = size;
      blockEntity.parentHash = parentHash;
      blockEntity.timestamp = timestamp;
      await blockEntity.save();

      const transactionsSave = [];
      for (let i = 0; i < transactions.length; i++) {
        const currentTransaction = transactions[i];
        const transactionsEntity = new Transactions();
        transactionsEntity.transactionId = currentTransaction.hash;
        transactionsEntity.from = currentTransaction.from;
        transactionsEntity.to = currentTransaction.to;
        transactionsEntity.value = currentTransaction.value;
        transactionsEntity.block = blockEntity;
        transactionsSave.push(transactionsEntity);
      }
      await this._repositoryTransactions.save(transactionsSave);

      this._loadedBlocks.add(blockEntity.blockNumber);
      this._processingBlocks.delete(id_hex);

      return `${blockEntity.blockNumber} - cached`;
    } catch (e) {
      this._logger.error(`${id_hex} GetBlockById: `, e.message);
      this._errorSyncBlocks.push(id_hex);
      this._processingBlocks.delete(id_hex);
    }
  }
}
