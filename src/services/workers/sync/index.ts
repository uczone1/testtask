import { pgConnect } from '../../../shared/utils/database-connect';
import { NestFactory } from '@nestjs/core';
import { SyncModule } from './sync-block.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

(async () => {
  await pgConnect();
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    SyncModule,
    { transport: Transport.TCP },
  );
  await app.listen();
})();
