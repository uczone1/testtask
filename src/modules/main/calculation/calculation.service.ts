import {
  BadRequestException,
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { AddressChangeDto } from './dto/address-change.dto';
import { CalculationPort } from '../../../domain/ports/calculation.port';
import { DatabaseCalculationAdapter } from './adapters/database-calculation.adapter';
import { ClientProxy } from '@nestjs/microservices';
import { NotReadyDto } from './dto/not-ready.dto';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class CalculationService {
  private readonly _adapterCalculation: CalculationPort;

  constructor(
    private _logger: Logger,
    @Inject('CALC_SERVICE') private _client: ClientProxy,
  ) {
    this._adapterCalculation = new DatabaseCalculationAdapter(_logger);
  }

  async getMaximumBalanceChanged(): Promise<AddressChangeDto | NotReadyDto> {
    try {
      const dbReady$ = await this._client.send<boolean>('dbReady', {});
      const dbReady = await firstValueFrom(dbReady$);
      if (!dbReady) {
        const leftToCache$ = await this._client.send<number>('leftToCache', {});
        const leftToCache = await firstValueFrom(leftToCache$);
        return new NotReadyDto(
          `Not ready, left to cache: ${leftToCache} blocks`,
        );
      }
    } catch (e) {
      throw new InternalServerErrorException('worker_is_down');
    }

    const addressChangeEntity =
      await this._adapterCalculation.getMaximumBalanceChanged();

    return new AddressChangeDto(addressChangeEntity);
  }
}
