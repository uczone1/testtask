import { CalculationPort } from '../../../../domain/ports/calculation.port';
import { AddressChangeEntity } from '../../../../domain/entities/address-change.entity';
import {
  Connection as DatabaseConnection,
  getConnection,
  Repository,
} from 'typeorm';
import { BadRequestException, Logger } from '@nestjs/common';
import { Blocks } from '../../../../shared/models/postgres/Blocks';
import { Transactions } from '../../../../shared/models/postgres/Transactions';
import BigNumber from 'bignumber.js';

export class DatabaseCalculationAdapter implements CalculationPort {
  protected _db: DatabaseConnection;
  protected _repositoryBlocks: Repository<Blocks>;

  constructor(private readonly _logger: Logger) {
    this.init();
  }

  private init() {
    this._db = getConnection();
    this._repositoryBlocks = this._db.getRepository(Blocks);
  }

  async getMaximumBalanceChanged(): Promise<AddressChangeEntity> {
    const lastBlockEntity = await this._repositoryBlocks
      .createQueryBuilder('block')
      .orderBy(`block."blockNumber"`, 'DESC')
      .getOne();

    const lastBlock = lastBlockEntity.blockNumber.toString();

    const minNumberBlock = '0x' + (parseInt(lastBlock, 16) - 100).toString(16);
    if (lastBlock) {
      const blocksEntity = await this._repositoryBlocks
        .createQueryBuilder('blocks')
        .where('blocks."blockNumber" > :minNumberBlock', { minNumberBlock })
        .leftJoinAndSelect('blocks.transactions', 'transactions')
        .getMany();

      const moneyChanged = {};
      for (let i = 0; i < blocksEntity.length; i++) {
        const currentBlock = blocksEntity[i];
        for (let j = 0; j < currentBlock.transactions.length; j++) {
          const currentTransaction: Transactions = currentBlock.transactions[j];

          const { from, to, value } = currentTransaction;

          const bigValue = new BigNumber(value);
          if (!moneyChanged.hasOwnProperty(from))
            moneyChanged[from] = new BigNumber(0);
          if (!moneyChanged.hasOwnProperty(to))
            moneyChanged[to] = new BigNumber(0);

          moneyChanged[from] = moneyChanged[from].minus(bigValue);
          moneyChanged[to] = moneyChanged[to].plus(bigValue);
        }
      }

      const keys = Object.keys(moneyChanged);
      const sortedAddress = keys.sort((a, b) => {
        return moneyChanged[a].abs().comparedTo(moneyChanged[b].abs());
      });

      const addressKey = sortedAddress[sortedAddress.length - 1];

      const result = new AddressChangeEntity();
      result.address = addressKey;
      result.changedValue = moneyChanged[addressKey]
        .dividedBy(Math.pow(10, 18), 10)
        .toString(10);
      result.firstBlock = minNumberBlock;
      result.lastBlock = lastBlock;

      return result;
    }
    throw new BadRequestException();
  }
}
