import { ApiProperty } from '@nestjs/swagger';

export class NotReadyDto {
  @ApiProperty()
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
