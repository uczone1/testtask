import { AddressChangeEntity } from '../../../../domain/entities/address-change.entity';
import { ApiProperty } from '@nestjs/swagger';

export class AddressChangeDto {
  @ApiProperty({ example: '0xcb604b' })
  firstBlock: string;

  @ApiProperty({ example: '0xcb60af' })
  lastBLock: string;

  @ApiProperty({ example: '0xea3ec2a08fee18ff4798c2d4725ded433d94151d' })
  address: string;

  @ApiProperty({ example: '-7817.390211 ETH' })
  changedValue: string;

  constructor(entity: AddressChangeEntity) {
    this.firstBlock = entity.firstBlock;
    this.lastBLock = entity.lastBlock;
    this.address = entity.address;
    this.changedValue = entity.changedValue + ' ETH';
  }
}
