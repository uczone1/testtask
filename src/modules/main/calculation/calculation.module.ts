import { forwardRef, Logger, Module } from '@nestjs/common';
import { CalculationService } from './calculation.service';
import { CalculationController } from './calculation.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      { name: 'CALC_SERVICE', transport: Transport.TCP },
    ]),
  ],
  controllers: [CalculationController],
  providers: [CalculationService, Logger],
})
export class CalculationModule {}
