import { Controller, Get, HttpCode } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CalculationService } from './calculation.service';
import { AddressChangeDto } from './dto/address-change.dto';
import { NotReadyDto } from './dto/not-ready.dto';

@ApiTags('calculation')
@Controller('calculation')
export class CalculationController {
  constructor(private _service: CalculationService) {}

  @ApiOperation({
    summary:
      'Получить адрес кошелька баланс которого изменился больше остальных (по абсолютной величине) за последние 100 блоков',
  })
  @Get('/')
  @HttpCode(200)
  @ApiOkResponse({ type: AddressChangeDto })
  async getMaximumBalanceChanged(): Promise<AddressChangeDto | NotReadyDto> {
    return await this._service.getMaximumBalanceChanged();
  }
}
