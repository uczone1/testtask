export class AddressChangeEntity {
  get firstBlock(): string {
    return this._firstBlock;
  }

  set firstBlock(value: string) {
    this._firstBlock = value;
  }

  get lastBlock(): string {
    return this._lastBlock;
  }

  set lastBlock(value: string) {
    this._lastBlock = value;
  }
  get address(): string {
    return this._address;
  }

  set address(value: string) {
    this._address = value;
  }

  get changedValue(): string {
    return this._changedValue;
  }

  set changedValue(value: string) {
    this._changedValue = value;
  }
  private _address: string;
  private _changedValue: string;
  private _firstBlock?: string;
  private _lastBlock?: string;
}
