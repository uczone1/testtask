import { AddressChangeEntity } from '../entities/address-change.entity';

export interface CalculationPort {
  getMaximumBalanceChanged?(): Promise<AddressChangeEntity>;
}
