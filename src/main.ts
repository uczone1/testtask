import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

import { pgConnect } from './shared/utils/database-connect';
import { appConfig } from './shared/config/config';

(async () => {
  await pgConnect();
  const app = await NestFactory.create(AppModule);
  const PORT = appConfig.app.port;
  const config = new DocumentBuilder()
    .setTitle('Test task')
    .setDescription('API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(PORT, () => Logger.log(`Server started on port = ${PORT}`));
})();
