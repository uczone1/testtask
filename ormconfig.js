const production = {
  type: 'postgres',
  synchronize: false,
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  entities: ['dist/src/shared/models/postgres/*'],
  logging: false,
  migrations: ['dist/src/shared/migration/postgres/*'],
  cli: {
    entitiesDir: 'src/shared/models',
    migrationsDir: 'src/shared/migration',
    subscribersDir: 'src/subscriber',
  },
};

const development = {
  type: 'postgres',
  synchronize: false,
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  entities: ['src/shared/models/postgres/*'],
  logging: false,
  migrations: ['src/shared/migration/postgres/*'],
  cli: {
    entitiesDir: 'src/shared/models',
    migrationsDir: 'src/shared/migration/postgres',
    subscribersDir: 'src/subscriber',
  },
};

module.exports = process.env.NODE_ENV === 'prod' ? production : development;
